// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "positionReport.h"


// Sets default values for this component's properties
UpositionReport::UpositionReport()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UpositionReport::BeginPlay()
{
	Super::BeginPlay();

	// ...

	FVector obVector = GetOwner()->GetTransform().GetLocation();
	FString location = obVector.ToString();
	float obX = obVector.X;

	FString objectName = GetOwner()->GetName();
	UE_LOG(LogTemp, Warning, TEXT("%s X position is: %f"), *objectName, obX);
	UE_LOG(LogTemp, Warning, TEXT("Position reported for %s at %s"), *objectName, *location);
	
}


// Called every frame
void UpositionReport::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

