// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "GrabComponent.h"


#define OUT


// Sets default values for this component's properties
UGrabComponent::UGrabComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	// ...
}


// Called when the game starts
void UGrabComponent::BeginPlay()
{
	Super::BeginPlay();

	// ... 
	UE_LOG(LogTemp, Warning, TEXT("Grabber reporting for duty"));
	
}


// Called every frame
void UGrabComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//Get Player Viewpoint
	FVector PlayerViewLocation;
	FRotator PlayerViewRotation;

	FHitResult RayHit;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(

		OUT PlayerViewLocation,
		OUT PlayerViewRotation
		
	
	);
	//UE_LOG(LogTemp, Warning, TEXT("Location: %s, Rotation: %s "), *PlayerViewLocation.ToString(), *PlayerViewRotation.ToString())

	//Draw RedLine to visualize pickup distance

	FVector LineEnd = PlayerViewLocation + PlayerViewRotation.Vector()*Reach;

	DrawDebugLine(
		GetWorld(),
		PlayerViewLocation,
		LineEnd,
		FColor(255,0,0),
		false,
		0.0f,
		0.0f,
		10.0f
	
	
	);
	//Set Up Query Parameters
	FCollisionQueryParams TraceParams(
		FName(TEXT("")),
		false,
		GetOwner()
	
	);
	// Raycast out to distance
	GetWorld()->LineTraceSingleByObjectType(
		OUT RayHit,
		PlayerViewLocation,
	    LineEnd,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParams
	
	
	);
	AActor* ActorHit = RayHit.GetActor();

	if (ActorHit) {
		UE_LOG(LogTemp, Warning, TEXT("Actor: %s"), *(ActorHit->GetName()))

			//Log every frame;
	}
}

