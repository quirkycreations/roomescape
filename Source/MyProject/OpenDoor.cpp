// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "OpenDoor.h"


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	ActorThatOpens=GetWorld()->GetFirstPlayerController()->GetPawn();
	Owner = GetOwner();
	// ...
	
}

void UOpenDoor::DoorOpen()
{
	FRotator NewRotation = FRotator(0.f, -80.f, 0.f);

	Owner->SetActorRotation(NewRotation);
}
void UOpenDoor::DoorClose() {
	FRotator NewRotation = FRotator(0.0f, 0.0f, 0.0f);
	Owner->SetActorRotation(NewRotation);
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Poll Actor Every Frame. If actor that opens is in the volume open door
	if (PressurePlate->IsOverlappingActor(ActorThatOpens)) {
		DoorOpen();
		DoorOpenedTime = GetWorld()->GetTimeSeconds();
	}

	
	//See if it's time to close the door;
	if (GetWorld()->GetTimeSeconds() >= DoorOpenedTime + DoorCloseDelay) {
		DoorClose();
	}
}

